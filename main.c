#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <unistd.h> 

// Our flawless Parsing Function
void parse(const uint8_t *Data, size_t Size)
{
	if (Size < 8) return;
	
	if (Data[0] == 'C')
	{
		if (Data[1] == 'r')
		{
			if (Data[2] == 'A')
			{
				if (Data[3] == 'S')
				{
					if (Data[4] == 'h')
					{
						if (Data[5] == 'H')
						{
							if (Data[6] == 'h')
							{
								// Crash #1 - CrASh!!!
								//*(char*)(0) = 0;
								return;
							}							
						}							
					}						
				}					
			}			
		}
	}
}

#ifdef FUZZING_MODE
int LLVMFuzzerTestOneInput(const uint8_t *Data, size_t Size) {
  parse(Data, Size);
  return 0;
}
#endif

#ifndef FUZZING_MODE
int main(int argc, char** argv) {

    if (argc < 2) {
        printf("Usage: ./FuzzApp <argument>\n");
        exit(1);
    }
	parse(argv[1], strlen(argv[1]));
	return 0;
}
#endif