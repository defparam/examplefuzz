all: release fuzz

release:
	@echo "release target"
	clang main.c -o ExampleApp
	
fuzz: fuzz_parse
	@echo "fuzz target"
	
fuzz_parse:
	clang main.c -DFUZZING_MODE -fsanitize=fuzzer,address -o ExampleApp-Fuzzer

clean:
	rm -f ExampleApp
	rm -f ExampleApp-Fuzzer
	rm -f crash-*